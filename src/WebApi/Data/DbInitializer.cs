﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoBogus.Templating;
using AutoBogus;
using WebApi.Models;

namespace WebApi.Data
{
    public class DbInitializer
    {
        public static void Initialize(ApplicationDbContext context)
        {
            if (context.Customers.Any())
            {
                return;
            }
            var customers =  new AutoFaker<Customer>().GenerateWithTemplate(@"
            Id|  Firstname | Lastname
            1|  John      | Smith
            2|  Jane      | Jones
            3|  Bob       | Clark
            ");
            context.Customers.AddRange(customers);
            context.SaveChanges();
        }
    }
}
