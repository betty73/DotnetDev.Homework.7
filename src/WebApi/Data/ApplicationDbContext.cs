﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using WebApi.Models;

namespace WebApi.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
          : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Customer>().Property(c => c.Id).ValueGeneratedOnAdd();
        }
        public Customer GetCustomer (long id)
        {
            return Customers.SingleOrDefault(p => p.Id == id);
        }
    }
}
