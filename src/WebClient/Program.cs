﻿using System;
using System.Threading.Tasks;
using Bogus;
using McMaster.Extensions.CommandLineUtils;
using System.Net.Http;
using System.Net.Http.Json;

namespace WebClient
{
    public class Program
    {
        static readonly HttpClient _httpClient = new ();
        static readonly Faker _faker = new ();
        static readonly string _url = Environment.GetEnvironmentVariable("url") ?? "https://localhost:5001";
        static async Task Main(string[] args) 
        {
            do
            {
                #region GetCustomer
                var client_id = Prompt.GetString("Enter ClientID:",
                           promptColor: ConsoleColor.Yellow);

                if (long.TryParse(client_id, out var _))
                {
                    try
                    {
                        var client_data = await GetCustomerAsync(client_id);
                        Console.WriteLine("Data by ClientID: {0}", client_data);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                else
                {
                    Console.WriteLine("Use correct number format!");
                    continue;
                }
                #endregion GetCustomer
                #region PostRandCustomer
                if (Prompt.GetYesNo("Create RandCustomer?", false, ConsoleColor.Yellow))
                {
                    try
                    {
                        var rand_customer = await PostCustomerAsync(RandomCustomer());
                        Console.WriteLine("Created RandCustomer: {0}", rand_customer);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                #endregion PostRandCustomer
            }
            while (!Prompt.GetYesNo("Want to exit?", false, ConsoleColor.Yellow));
        }

        private static CustomerCreateRequest RandomCustomer()
        {
            return new CustomerCreateRequest(_faker.Name.FirstName(), _faker.Name.LastName());
        }
        private static async Task<Customer> GetCustomerAsync(string? _id)
        {
            try
            {
                var response = await _httpClient.GetStringAsync($"{_url}/customers/{_id}");
                return Newtonsoft.Json.JsonConvert.DeserializeObject<Customer>(response);
            }
            catch (HttpRequestException ex) when (ex.StatusCode==System.Net.HttpStatusCode.NotFound)
            {
                throw new Exception("Client with such Id is not found");
            }
        }
        private static async Task<Customer> PostCustomerAsync(CustomerCreateRequest _data)
        {
            try
            {
                var response = await _httpClient.PostAsJsonAsync($"{_url}/customers",_data);
                var client_id = await response.Content.ReadAsStringAsync();
                return await GetCustomerAsync(client_id);
            }
            catch (HttpRequestException ex) when (ex.StatusCode == System.Net.HttpStatusCode.Conflict)
            {
                throw new Exception("Client with such Id already exists");
            }
        }
    }
}